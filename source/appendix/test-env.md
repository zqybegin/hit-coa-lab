# 处理器测试环境

为了帮助各位同学更好的debug，实验二和实验三具有[测试环境](#TODO:)，可对CPU进行验证。

## 使用要求

要求每个同学的CPU对外暴露以下接口（信号名称与位数均不可改变）：

```verilog
module mycpu (
    input            clk             ,  // clock, 100MHz
    input            rst_n           ,  // active low

    output           inst_req        ,  // instruction request
    output [31:0]    inst_addr       ,
    input  [31:0]    inst_data       ,

    output           data_req        ,  // data request
    output [31:0]    data_addr       ,
    output           data_wr         ,  // data write enable
    output [31:0]    data_wdata      ,
    input  [31:0]    data_rdata      ,

    // debug signals
    output [31:0]    debug_wb_pc     ,  // 写回段的PC
    output [3 :0]    debug_wb_rf_wen ,  // 写回段的寄存器每字节写使能
    output [4 :0]    debug_wb_rf_wnum,  // 写回段需要写回的寄存器编号
    output [31:0]    debug_wb_rf_wdata  // 写回段写回的数据
);
    /*TODO*/
endmodule
```

```{warning}
* 只允许按照给定的接口格式去设计CPU，不允许更改接口格式
* 所有信号在时钟上升沿采样，在写使能为0时，`debug_wb_rf_addr`、`debug_wb_rf_wdata`可以为任意值
* 仅需添加待完成的CPU，其他部分不要修改
```

## 仿真结果

当所实现的CPU功能正确时，会在控制台打印**PASS**，如下图所示。

```{image} ../_static/img/success.png
:alt: fig2
:width: 500px
:align: center
```

当所实现的CPU出现错误时，会在控制台打印错误信息，如下图所示。

其中 Reference 一行打印的是正确的CPU的输出信息，Error 一行打印的是你所实现的CPU的输出信息。

```{image} ../_static/img/error.png
:alt: fig2
:width: 500px
:align: center
```
