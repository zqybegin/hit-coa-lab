# 参考资料
## MIPS手册

* {download}`MIPS Volume I - Introduction to the MIPS32 Architecture.rev3.02 <../pdf/MIPS Volume I - Introduction to the MIPS32 Architecture.rev3.02.pdf>`
* {download}`MIPS Volume II - The MIPS32 Instruction Set.rev3.02 <../pdf/MIPS Volume II - The MIPS32 Instruction Set.rev3.02.pdf>`
* {download}`MIPS Volume III - The MIPS32 and microMIPS32 Privileged Resource Architecture.rev3.12 <../pdf/MIPS Volume III - The MIPS32 and microMIPS32 Privileged Resource Architecture.rev3.12.pdf>`

## 龙芯开发板手册

关于Vivado和龙芯开发板的使用，龙芯杯团队赛发布包里的文档已经足够详细，请参考。

* {download}`A03_“系统能力培养大赛”MIPS指令系统规范_v1.01 <../pdf/A03_“系统能力培养大赛”MIPS指令系统规范_v1.01.pdf>`
* {download}`A04_实验箱介绍 <../pdf/A04_实验箱介绍.pdf>`
* {download}`A05_实验箱原理图 <../pdf/A05_实验箱原理图.pdf>`
* {download}`A06_vivado安装说明_v1.00 <../pdf/A06_vivado安装说明_v1.00.pdf>`
* {download}`A07_vivado使用说明_v1.00 <../pdf/A07_vivado使用说明_v1.00.pdf>`
* {download}`A08_交叉编译工具链安装_v1.00 <../pdf/A08_交叉编译工具链安装_v1.00.pdf>`
* {download}`A09_CPU仿真调试说明_v1.00 <../pdf/A09_CPU仿真调试说明_v1.00.pdf>`
* {download}`A10_FPGA在线调试说明_v1.00 <../pdf/A10_FPGA在线调试说明_v1.00.pdf>`
* {download}`A11_Trace比对机制使用说明_v1.00 <../pdf/A11_Trace比对机制使用说明_v1.00.pdf>`

## 实验版手册

* {download}`Nexys 3™ FPGA Board Reference Manual <../pdf/nexys3_rm.pdf>`
* {download}`Nexys 3™ FPGA Board Schematic <../pdf/nexys3_sch.pdf>`

