.. Copyright 2021, Harbin Institute of Technology

计算机组成原理（2021秋）
=======================================

PDF版下载：`下载链接 <./manual.pdf>`__

校内镜像站：`hit-coa-lab <https://mirrors.hit.edu.cn/hit-coa-lab/>`__ （感谢 `@HITLUG <https://lug.hit.edu.cn/>`__ ）

-----

实验课时间：

实验分数构成：

教师及助教信息：

-----

.. used to get the title of TOC right in generated pdf
.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: 目录

.. toctree::
   :maxdepth: 1
   :numbered: 3
   :caption: 教程

   tutorial/verilog
   tutorial/dev_env
   tutorial/dev_step

.. toctree::
   :maxdepth: 1
   :numbered: 3
   :caption: 实验

   labs/overview
   labs/lab1
   labs/lab2
   labs/lab3
   labs/lab4
   labs/lab-extra

.. toctree::
   :maxdepth: 1
   :numbered: 3
   :caption: 附录

   appendix/faqs
   appendix/test-env
   appendix/reference
