# 实验三 流水线处理器

## 实验目的

* 掌握 Xilinx ISE 集成开发环境
* 掌握 Verilog 语言
* 掌握 FPGA 编程方法及硬件调试手段
* 深刻理解流水线型处理器结构和数据冲突解决技术的工作原理

## 实验环境

* Xilinx ISE 集成开发环境和 Nexys 3 实验平台

## 实验内容

* 根据本课程所讲的设计思想，在实验二实现的CPU的基础上，设计并实现一个具有五段流水线处理器，并通过暂停和定向（或旁路）技术来解决流水线中出现的数据冲突，要求该处理器可以**通过所提供的自动测试环境**。
* 该处理器**需要处理延迟槽指令**，但不需要处理异常。

## 实验要求

按照以上要求将实验二所实现的单周期处理器改进成具有五段流水线处理器，并通过暂停和定向技术解决流水线中产生的数据冲突。

###  实验预习

在实验开始前给出处理器的设计方案，设计方案要求包括：

* 指令格式设计 
* 处理器结构设计框图（要求精确到信号以及信号的位）及功能描述 
* 各功能模块结构设计框图及功能描述
* 各模块输入输出接口信号定义（以表格形式给出）

```{warning}
处理器结构设计框图要求精确到：每一个子模块的信号以及信号的位。
```

### 完成实验内容

**Verilog 语言实现**：要求采用结构化设计方法，用 Verilog 语言实现处理器的设计。设计包括：

  * 各模块的详细设计（包括各模块功能详述，设计方法，Verilog 语言实现等）
  * 各模块的功能测试（每个模块作为一个部分，包括测试方案、测试过程和测试波形等） 
  * 系统的详细设计（包括系统功能详述，设计方法，Verilog 语言实现等）
  * 系统的功能测试（包括系统整体功能的测试方案、测试过程和测试波形等）

**FPGA 编程下载**：调用 Xilinx 系统的 iMPACT 将 VerilogHDL 程序下载到 Nexys3 实验板中。然后利用 Xilinx ISE 系统的 ChipScope 观察 Nexys 3 实验板的 FPGA 芯片中的实际运行，观察处理器 内部运行状态，显式输出内部状态运行结果。 

**对处理器进行功能测试，记录运行过程和结果，完成实验报告**：对处理器进行功能测试，编写处理器功能测试程序，包括指令缓冲存储器和数据缓冲存储器完成处理器功能测试，并观察记录运行过程和结果，完成实验报告。

## 实验提示

### 流水线的控制模式概述

流水线需要暂停，当某个流水线段被阻塞时，前面的流水段也需要被阻塞。这是因为：当某个流水段暂停时，此流水段没有办法接受新的数据，前面的流水段必须把原本的数据保持在本流水线段中，否则，如果数据继续向后流动，就会造成数据丢失，在没有特殊机制处理的情况下，就会发生错误。

控制流水线的阻塞，**核心在于控制好各级流水线段间寄存器的写使能信号**。

### 流水线级间互锁机制

**<font size = 4>核心思想</font>**

核心思想：为每一级流水线分配一个监管人员。它和前后级流水线的监管人员相互沟通情况（前级流水段：是否有数据传入；后级流水段：是否可以接受数据），决定下一时刻是否传送数据。如果后继流水段不可以接受数据，而且此时该流水段中有有效数据保存，就不可以接受前级流水段的数据输入。流水线中个流水段环环相扣，使得整个流水线正常运行。

**<font size = 4>例程代码</font>**

这是一个具有三段流水段的例程，其相邻流水级之间仅有如图所示的四个信号：

* `stageX_allowin`：流水段X传递给流水段X-1的信号，该信号有效说明下一拍流水段X可以接受流水段X-1的数据。
* `stageX_bus_r`：流水段X-1和流水段X之间的段间寄存器传递给流水线X的数据信号，该数据信号为上一流水级X-1传递给当前流水级X的数据，但未经过当前流水级X的处理。
* `stageX_to_stageX+1_valid`：流水段X传递给流水段X+1的信号，该信号有效说明此时流水段X有数据需要在下一周期传递给流水段X+1。
* `stageX_to_stageX+1_bus`：流水段X之间的段间寄存器传递给流水线X+1的数据信号，该信号为流水段X传递给流水段X+1的数据。

```{image} ../_static/img/pipeline_distribute.jpg
:width: 100%
:align: center
```

此处，仅仅给出包含 stage2 的 pipeline2.v 模块。

````{note}
在 pipelineX.v 模块中，将包含 stageX 和 stageX前的段间寄存器。
```{image} ../_static/img/pipeline_module_distribute.jpg
:width: 100%
:align: center
```
````

```verilog
//pipeline2.v
module pipeline_2(
    input clk,
    input rst,

    //需要在流水段间传递的控制信号
    input stage3_allowin_in,
    output stage2_allowin_out,
    //来自于pipeline2的信号
    input stage1_to_stage2_valid_in,
    input stage1_to_stage2_bus_in,
    //pipeline2传递到pipeline3的信号
    output stage2_to_stage3_valid_out,
    output stage2_to_stage3_bus_out
    );
    //*******************************************************************
    //                  stage1 和 stage2 之间段间寄存器
    //*******************************************************************
    reg stage2_valid_r;
    reg stage2_bus_r;

    wire stage2_allowin;
    wire stage2_ready_go;
    wire stage2_to_stage3_valid;

    assign stage2_ready_go 	= /*TODO*/;
    assign stage2_allowin 	= !stage2_valid_r || stage2_ready_go && stage3_allowin_in;
    assign stage2_to_stage3_valid	= stage2_valid_r && stage2_ready_go;

    /*
    *   stage2_ready_go信号说明：
    *       描述stage2当前拍状态的信号，该信号有效说明此时stage2的处理任务已经完成，可以向后一流水段传递数据。
    *       由于在部分流水段中，单个周期可能无法完成该阶段的信号转换，故而设置了该信号。
    *   stage2_ready_go信号赋值说明：
    *       TODO部分需要填充的是stage2的信号完成标志信号，如果可以确定当前流水段一定在单周期内完成信号转换，则可恒置为1。
    *   
    *   stage2_allowin信号说明：
    *       stage2传递给stage1的信号，该信号有效说明下一拍stage2可以接受stage1的数据。
    *   stage2_allowin信号赋值说明：
    *       如果当前流水段为null（段间寄存器内不存在任何数据），那么该信号置为有效。
    *       如果当前流水段不为null（段间寄存器内存在数据），且已知该流水段的信号已经处理完毕，且下一流水段可以接受数据，那么该信号置为有效。
    *
    *   stage2_to_stage3_valid信号说明：
    *       stage2传递给stage3的信号。该信号有效说明此时stage2有数据需要在下一周期传递给stage3。
    *   stage2_to_stage3_valid信号赋值说明：
    *       如果当前流水段不为null（段间寄存器内存在数据），且已知该流水段的信号已经处理完毕，那么该信号置为有效
    */
    always @(posedge clk) begin
        if (rst)begin
            stage2_valid_r <= 0;
        end else if(stage2_allowin) begin
            stage2_valid_r <= stage1_to_stage2_valid_in;
        end

        if(stage2_allowin && stage1_to_stage2_valid_in)begin
            stage2_bus_r <= stage1_to_stage2_bus_in;
        end
    end

    //*******************************************************************
    //                             stage2 
    //*******************************************************************

    //TODO:需要完成的信号转换 stage2_bus_r >> stage2_to_stage3_bus

    //*******************************************************************
    //                           输出信号
    //*******************************************************************
    assign stage2_allowin_out         = stage2_allowin;
    assign stage2_to_stage3_valid_out = stage2_to_stage3_valid;
    assign stage2_to_stage3_bus_out   = stage2_to_stage3_bus;

endmodule
```

### 流水线集中控制机制

**<font size = 4>核心思想</font>**

通俗意义上来讲，核心思想为：**配备一个生产流水线监管人员**。可以同时观察到各级流水线的状态，并向各级流水线下达命令。一旦某一时刻某个流水段阻塞，就立即给出前面流水段停止向后传送数据的命令。

**<font size = 4>例程代码</font>**

这是一个具有三段流水段的例程，除了流水级以及段间寄存器之外，还存在一个控制模块ctrl，该模块控制流水线段间寄存器的读写，其信号说明如下：

* `stall_from_X`：每一级流水线发送到总控制模块ctrl的控制信号。该信号高有效说明，下一周期时，流水段X的处理任务未完成，不可以向流水段X+1传递数据，需要暂停。
* `stall[X]`：总控制模块ctrl发送到每一级流水线的控制信号。该信号高有效说明，下一拍时，流水段X需要暂停。

```{image} ../_static/img/pipeline_centrol.jpg
:width: 100%
:align: center
```

此处，仅仅给出包含 stage2 的 pipeline2.v 模块 以及 ctrl.v 模块。

````{note}
在 pipelineX.v 模块中，将包含 stageX 和 stageX前的段间寄存器。
```{image} ../_static/img/pipeline_module_centrol.jpg
:width: 100%
:align: center
```
````

```verilog
//ctrl.v
module ctrl(
    input rst,

    input stall_from_1_in,
    input stall_from_2_in,
    input stall_from_3_in,
    output [2:0]stall_out,
    );

    assign stall_out = 	(stall_from_3_in)?  3'b111  //stage3 暂停，{ stage1，stage2，stage3 }均暂停
                        (stall_from_2_in)?  3'b011: //stage2 暂停，{ stage1，stage2 }均暂停
                        (stall_from_1_in)?  3'b001: //stage1 暂停，{ stage1 }均暂停
                                            3'b000; //无暂停

endmodule
```

```verilog
//pipeline_2.v
module pipeline_2(
    input clk,
    input rst,

    input [2:0]stall_in,

    input stage1_to_stage2_bus_in,
    output stage2_to_stage3_bus_out,
    output stall_from_2_out
    );

    //*******************************************************************
    //                  stage1 和 stage2 之间段间寄存器
    //*******************************************************************
    reg stage2_bus_r;

    /*
    * stall_in[x]高有效，stall_in[x+1]低有效：流水段X发生暂停，流水段X+1正常向后流动，需要将二者之间的段间寄存器置为0。
    * stall_in[x]高有效，stall_in[x+1]高有效：流水段X+1或流水段X+1之后的流水段发生暂停，不应改变二者之间寄存器的值。
    * stall_in[x]低有效，stall_in[x+1]低有效：流水线正常流动。
    * stall_in[x]低有效，stall_in[x+1]高有效：不可能出现。
    */
    always @(posedge clk) begin
        if (rst)begin
            stage2_bus_r <= 0;
        end else if( stall_in[1] && !stall_in[2]) begin
            stage2_bus_r <= 0;
        end else if(!stall_in[1]) begin
            stage2_bus_r <= stage1_to_stage2_bus_in;
        end
    end

    //*******************************************************************
    //                             stage2 
    //*******************************************************************

    //TODO:需要完成的信号转换 stage2_bus_r >> stage2_to_stage3_bus

    //*******************************************************************
    //                           输出信号
    //*******************************************************************
    assign stall_from_2_out         = /*TODO*/;
    assign stage2_to_stage3_bus_out = stage2_to_stage3_bus;

endmodule
```
