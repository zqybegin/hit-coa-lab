# 实验二 给定指令系统的处理器设计

## 实验目的

* 掌握 Xilinx ISE 集成开发环境
* 掌握 Verilog 语言
* 掌握 FPGA 编程方法及硬件调试手段
* 深刻理解处理器结构和计算机系统的整体工作原理

## 实验环境

* Xilinx ISE 集成开发环境和 Nexys 3 实验平台

## 实验内容

* 根据课程第五章 MIPS 指令集的基本实现，设计并实现一个符合实验指令的**非流水**处理器， 包括 Verilog 语言的实现和 FPGA 芯片的编程实现，要求该处理器可以**通过所提供的自动测试环境**。
* 该处理器不需要处理延迟槽指令，异常。

## 实验要求

###  实验预习

在实验开始前给出处理器的**设计方案**，设计方案要求包括：

* 指令格式设计
* 处理器结构设计框图（要求精确到信号以及信号的位）及功能描述 
* 各功能模块结构设计框图及功能描述
* 各模块输入输出接口信号定义（以表格形式给出）

```{warning}
处理器结构设计框图要求精确到：每一个子模块的信号以及信号的位。
```

### 完成实验内容

**Verilog 语言实现**：要求采用结构化设计方法，用 Verilog 语言实现处理器的设计。设计包括：

  * 各模块的详细设计（包括各模块功能详述，设计方法，Verilog 语言实现等）
  * 各模块的功能测试（每个模块作为一个部分，包括测试方案、测试过程和测 试波形等） 
  * 系统的详细设计（包括系统功能详述，设计方法，Verilog 语言实现等） 
  * 系统的功能测试（包括系统整体功能的测试方案、测试过程和测试波形等）

**FPGA 编程下载**：调用 Xilinx 系统的 iMPACT 将 VerilogHDL 程序下载到 Nexys3 实验板中。然后利用 Xilinx ISE 系统的 ChipScope 观察 Nexys 3 实验板的 FPGA 芯片中的实际运行，观察处理器 内部运行状态，显式输出内部状态运行结果。 

**对处理器进行功能测试，记录运行过程和结果，完成实验报告**：对处理器进行功能测试，编写处理器功能测试程序，包括指令缓冲存储器和数据缓冲 存储器完成处理器功能测试，并观察记录运行过程和结果，完成实验报告。

## 实验提示

### CPU总体设计

在进行CPU的总体设计时，需要着重考虑的一点是“**数据通路的复用**”。

毫无疑问，指令系统中的每条指令都需要一条数据通路来完成该指令的功能，不同指令的数据通路之间存在重合部分。如果一个指令系统中的每条指令所需要的数据通路都不存在重合，那么无疑这个指令系统的设计是存在问题的，本次实验所给出的指令系统是不存在该问题的。

### 通用寄存器的设计

在单发射顺序CPU中，通用寄存器只需要两个读端口和一个写端口。通常使用异步读，同步写的时序。

```verilog
module regfile(
    input         clk   ,
    input		  rst_n ,
    // READ PORT 1
    input  [4 :0] raddr1,
    output [31:0] rdata1,
    // READ PORT 2
    input  [4 :0] raddr2,
    output [31:0] rdata2,
    // WRITE PORT
    input         we    ,   //write enable, active high
    input  [4 :0] waddr ,
    input  [31:0] wdata
);
    reg [31:0] rf [31:1];

    //WRITE
    integer i;
    always @(posedge clk) begin
        if (rst_n) begin
            for(i = 1; i < 32; i = i + 1) begin
                regs[i] <= 32'b0; //将所有寄存器初始化为0
            end
        end
        else if (we && |waddr) begin
            rf[waddr] <= wdata;
        end
    end

    //READ OUT 1
    assign rdata1 = (raddr1 == 5'b0) ? 32'b0 : rf[raddr1];

    //READ OUT 2
    assign rdata2 = (raddr2 == 5'b0) ? 32'b0 : rf[raddr2];

endmodule
```
