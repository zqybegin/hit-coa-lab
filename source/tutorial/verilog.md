# Verilog简介

Verilog是一种用于描述、设计电子系统（特别是数字电路）的硬件描述语言。

然而在硬件设计中，测试/验证所设计硬件的正确性也是很重要的环节，因此Verilog有一部分语法是方便做验证的，不能进行综合实现到硬件上。

所以学习Verilog，主要学习2个部分：

* 可综合(Synthesizable)代码编写，这部分的代码能映射到硬件上
* 测试(Testbench)代码编写，可以使用Verilog全部的语法，方便验证设计的正确性

具体什么代码可以综合，请参考Vivado Synthesis Guide。

## 可综合代码设计

### Verilog四值逻辑系统

Verilog的逻辑系统中有四种值，也即四种状态。

* 逻辑 0：表示低电平；
* 逻辑 1：表示高电平；
* 逻辑 X：表示未知，有可能是高电平，也有可能是低电平；
* 逻辑 Z：表示高阻态，通常是没有激励信号造成的；

在仿真时，如果观察到一个信号为X，这常常是驱动它的信号中有Z或者X造成的。

### Verilog变量类型

Verilog所用到的所有变量都属于两个基本的类型：线网(wire)类型和寄存器(reg)类型。

#### 线网(wire)

线网与我们实际使用的电线类似，它的数值一般只能通过连续赋值，由赋值符右侧连接的驱动源决定。

例如：

```verilog
wire a, b, c;           // 定义3个wire类型变量
assign a = b & c;       // a被(b & c)持续驱动，当(b & c)变化时，（不考虑延迟）a会同步变化
                        // 当然b和c作为wire类型，也需要被驱动
wire [1:0] d = {b, c};  // 定义1个宽度为2的wire类型变量并同时赋值
```

#### 寄存器(reg)

寄存器与之不同，它可以保存当前的数值，直到某个时机另一个数值被赋值给它。如果未对寄存器变量赋值，它的初始值则为X。

Verilog中的寄存器(reg)和硬件上的寄存器不能完全等价，具体区别在之后说明。

例如：

```verilog
reg        valid;       // 宽度为1的reg变量
reg [1 :0] data_size;   // 宽度为2的reg变量
reg [31:0] data;        // 宽度为64的reg变量
```

#### 变量类型的选择

wire类型还是reg类型，主要是根据自己需求选择。但需要符合一定的规定：

* 模块的输入端口只能是wire，但可以被wire/reg驱动
* 模块的输出端口可以是wire/reg，但是只能驱动外界的wire。

#### 数字的表示

对变量赋值时，需要使用下述数字表示方法。

基本格式：<位宽>'<数制的符号><数值>

* 位宽是与数据大小相等的对应二进制数的位数加上占位所用0的位数，这个位数需要使用十进制来表示。
* 如果省略位宽，那么实际宽度与综合工具实现有关。
* 数制需要用字母来表示，h对应十六进制，d对应十进制，o对应八进制，b对应二进制。

例如：

| 例子                | 说明                        |
| ------------------- | --------------------------- |
| `233          ` | 10进制数233                 |
| `8'hff        ` | 8位16进制数FF               |
| `32'd1234     ` | 32位10进制数1234            |
| `3'b010       ` | 3位2进制数10                |
| `32'habcd_1234` | 32位16进制abcd1234          |
| `8'hz         ` | 8位高阻态，可以赋给wire类型 |

#### 向量

向量形式的数据是Verilog相对C语言较为特殊的一种数据。

向量和C语言的数组不能等价，向量可以直接进行加减等运算。

```verilog
reg  [7:0] data;
wire [7:0] a, b, c; // 声明3个向量a, b, c
assign a = 8'd1;
assign b = 8'd2;
assign c = a + b;   // c == 8'd3

wire [3:0] a_high = a[7:4]; // a的高4位
wire [3:0] a_low  = a[3:0]; // a的低4位
```


#### 数组

声明数组时，和声明向量不同，方括号位于数组名的后面，括号内的第一个数字为第一个元素的序号，第二个数字为最后一个元素的序号，中间用冒号隔开。

如果数组是由向量构成的，则数组的其中某个元素是向量。

```verilog

wire       array     [15:0]; // 一维数组
wire [7:0] vec_array [15:0]; // 一维数组
wire [7:0] mat       [15:0][15:0]; // 二维数组
reg        reg_array [15:0]; // reg构成的一维数组

assign array[0]     = 1'b1;
assign vec_array[0] = 16'd2;
assign mat[0][9]    = 8'd3;

always @(posedge clk) begin
    ...
    reg_array <= 16'hffff;
    ...
end
```

#### 参数

可以通过`parameter`关键字声明参数以增强模块可拓展性和可读性。参数与常数的意义类似，不能够通过赋值运算改变它的数值。

在模块实例化时，可以使用`#()`将所需的实例参数覆盖模块的默认参数。

局部参数可以用`localparam`关键字声明，它不能够进行参数重载。

例如，我们声明一个宽度可变的加法器：

```verilog
module adder #(
    parameter WIDTH = 4     // 默认宽度为4
) (
    input  [WIDTH - 1 : 0] a,
    input  [WIDTH - 1 : 0] b,
    output [WIDTH - 1 : 0] c
);
    assign c = a + b;
endmodule
```

在我们例化这个模块时，可以进行如下操作：

```verilog
// 覆盖宽度，修改为6
adder #( .WIDTH (6) ) U_adder_0(
    .a (a ),
    .b (b ),
    .c (c )
);

// 使用默认的宽度4
adder U_adder_1(
    .a (a ),
    .b (b ),
    .c (c )
);
```

局部参数`localparam`和参数类似，但是不能在例化时被覆盖。

局部参数样例：

```verilog
// 声明一个乘法器
module mult (
    [port list], ...
)

reg [1:0] state;    // 乘法器有3个状态

localparam IDLE = 2'd0;
localparam CALC = 2'd1;
localparam DONE = 2'd2;

...
// 参数在模块内可以直接引用
wire mult_ready = (state == IDLE);
...

endmodule
```

### 运算

Verilog的许多运算符和C语言类似，但是有一部分运算符是特有的，例如拼接运算符、缩减运算符、带有无关位的相等运算符等。

#### 按位运算

* 按位取反`~`：1个多位操作数按位取反。例如：`a = 4'b1011`，则`~a`的结果为`4'b0100`
* 按位与`&`：2个多位操作数按位进行与运算，各位的结果按顺序组成一个新的多位数。例如：`a = 2'b10`，`b = 2'b11`，则`a & b`的结果为`2'b10`
* 按位或`|`：2个多位操作数按位进行或运算，各位的结果按顺序组成一个新的多位数。例如：`a = 2'b10`，`b = 2'b11`，则`a | b`的结果为`2'b11`
* 按位异或`^`：2个多位操作数按位进行异或运算，各位的结果按顺序组成一个新的多位数。例如：`a = 2'b10`，`b = 2'b11`，则`a ^ b`的结果为`2'b01`

#### 逻辑运算

* 逻辑取反`!`：对1个操作数进行逻辑取反。
* 逻辑与`&&`：对2个操作数进行逻辑与。
* 逻辑或`||`：对2个操作数进行逻辑或。

#### 缩减运算

* 缩减与`&`：对一个多位操作数进行缩减与操作，计算所有位之间的与操作结果，例如：`&(4'b1011)`的结果为`0`
* 缩减或`|`：对一个多位操作数进行缩减或操作，计算所有位之间的或操作结果。例如：`|(4'b1011)`的结果为`1`
* 缩减异或`^`：对一个多位操作数进行缩减异或操作，计算所有位之间的异或操作结果。例如：`^(4'b1011)`的结果为`1`

缩减或非、缩减异或、缩减同或是类似的。

#### 算术运算

* 加`+`：2个操作数相加
* 减`-`：2个操作数相减或取1个操作数的负数
* 乘`*`：2个操作数相乘
* 除`/`：2个操作数相除
* 求余`%`：2个操作数求余

#### 关系运算

* 大于`>`：比较2个操作数，如果前者大于后者，结果为真
* 小于`<`：比较2个操作数，如果前者小于后者，结果为真
* 大于或等于`>=`：比较2个操作数，如果前者大于或等于后者，结果为真
* 小于或等于`<=`：比较2个操作数，如果前者小于或等于后者，结果为真
* 逻辑相等`==`：2个操作数比较，如果各位均相等，结果为真。如果其中任何一个操作数中含有x或z，则结果为x
* 逻辑不等`!=`：2个操作数比较，如果各位不完全相等，结果为真。如果其中任何一个操作数中含有x或z，则结果为x

#### 移位运算

* 逻辑右移`>>`：1个操作数向右移位，产生的空位用0填充
* 逻辑左移`<<`：1个操作数向左移位，产生的空位用0填充
* 算术右移`>>>`：1个操作数向右移位。如果是无符号数，则产生的空位用0填充；有符号数则用其符号位填充
* 算术左移`<<<`：1个操作数向左移位，产生的空位用0填充

#### 其他运算

* 拼接`{,}`：2个操作数分别作为高低位进行拼接，例如：`{2'b10,2'b11}`的结果是`4'b1011`
* 重复`{n{m}}`：将操作数`m`重复`n`次，拼接成一个多位的数。例如：`a=2'b01`，则`{2{a}}`的结果是`4'b0101`
* 条件`? :`：根据`?`前的表达式是否为真，选择执行后面位于`:`左右两个语句。例如：`assign c = (a > b) ? a : b`，如果`a`大于`b`，则将`a`的值赋给`c`，否则将`b`的值赋给`c`


```{note}
1. 使用拼接可以实现循环左移或者右移，如：`assign c = {a[0], a[7:1]}`是一个循环右移
2. 注意优先级，与运算优先级总是大于或运算
3. 有两个操作符的运算尽量保证二者宽度相等，不满足条件时可以用拼接运算手动添0补齐
4. 嵌套的条件运算可以容易地实现组合逻辑，但是要注意优先级
5. 可以用拼接和重复实现符号拓展：如拓展一个8位有符号数`imm`至16位：`{8{imm[7]}, imm}`
```

### 组合逻辑

组合逻辑可由2种方法表示：`assign`语句和`always`块。

#### assign语句

`assign`语句是对线网类型变量的赋值。线网不能够像寄存器那样储存当前数值，它需要驱动源提供信号，这种驱动是连续不断的，因此线网变量的赋值称为连续赋值。

对同一个wire变量赋值的`assign`语句只能有一条。

样例：

```verilog
wire [1:0] a;
wire [1:0] b;
wire [3:0] c = 4'b1011;
wire       d;
wire       e;

assign {a, b} = c;  // a = 2'b10, b = 2'b11
assign d = c[0] ? a[0] :
           c[1] ? a[1] : 1'b0;
assign e = a[0] & b[0];
```

#### always块

**不推荐**使用`always`块来表示组合逻辑。不正确的使用会生成大量锁存器。

下面的例子是一个32-5优先编码器，如果`tlb_hit_array`全为0，那么`tlb_hit_index`也为0。

`always`块中被赋值的变量只能是`reg`类型，但是该编码器并不会综合出寄存器或者锁存器，这是因为Verilog中的寄存器(reg)和硬件上的寄存器不能完全等价。

如果去掉`tlb_hit_index = 5'd0;`一句，则会综合出锁存器。

```verilog
reg  [4 :0] tlb_hit_index;
wire [31:0] tlb_hit_array;

integer i;
always @(*) begin
    tlb_hit_index = 5'd0;
    for (i = 0; i < 32; i = i + 1) begin
        if (tlb_hit_array[i]) begin
            tlb_hit_index = i;
        end
    end
end
```

```{warning}
1. 建议大家使用`assign`表示组合逻辑，这种做可以保证不会综合出锁存器
2. 要避免写出组合逻辑环，也就是`assign a = b; assign b = ~a`这种代码
```

```{note}
思考：为什么不要让代码综合出不必要的锁存器？（时序分析、资源占用、需求等等）
```

### 时序逻辑

时序逻辑是在`always`块中描述的，被赋值的变量一定要是`reg`类型。

下面的例子可以在时钟上升沿交换a, b的值：

```verilog
reg a, b;

always @ (posedge clk) begin
    a <= b;
    b <= a;
end
```

上个例子中，寄存器没有做复位，这样实现的时候寄存器初值是不固定的。

```verilog
reg a, b;

always @ (posedge clk) begin
    if (~rst_n) begin   // 低电平有效
        a <= 1'b0;
        b <= 1'b0;
    end else if (...) begin
        ...
    end
    else begin
        a <= b;
        b <= a;
    end
end
```

需要注意，对一个寄存器变量的赋值一定要在一个`always`块中，不要写出多驱动代码。否则就算仿真时能波形正常，但是综合时也会失败。

在Verilog中，有两种赋值运算，一种叫做阻塞赋值（blocking assignment），其运算符为`=`；另一种叫做非阻塞赋值（non-blocking assignment），其运算符为`<=`。

在顺序代码块中使用阻塞赋值语句，如果这一句没有执行完成，那么后面的语句不会执行；如果在顺序代码块中使用非阻塞赋值，则执行这一句的同时，并不会阻碍下一句代码的执行。在时序逻辑中应使用非阻塞赋值。

```{warning}
1. 我们建议使用同步复位，也就是敏感列表中只有`posedge clk`
2. 在时序逻辑中应使用非阻塞赋值
3. 避免写出多驱动的代码，对一个寄存器变量的赋值一定要在一个`always`块中
4. 建议使用复位信号实现对寄存器的初始化，对大块内存（指令内存或数据内存）的初始化参考Vivado/ISE初始化Block Memory教程
```

### 条件语句

表示条件可以以下3种方法：

#### 条件运算符 ? : 

使用条件运算符表示**组合**逻辑。例如：`assign data_out = souce_a ? data_a : data_b;`

条件运算符可以嵌套使用：例如：

```verilog
// 有优先级的4选1 MUX
assign data_out = source_a ? data_a :
                  source_b ? data_b :
                  source_c ? data_c : data_d;
```

#### if-else语句

if—else语句常用于描述**时序**逻辑中。例如这个例子：

```verilog
always @(posedge clk) begin
    if (~rst_n) begin
        a <= 1'b0;
    end else if (src_b) begin
        a <= b;
    end else if (src_c) begin
        a <= c;
    end
end
```
时钟上升沿`src_b`和`src_c`都为0时，`a`会保持自己的值。

#### case语句

常用于描述**时序**逻辑，语法和C语言类似，例如如下的自动机：

```verilog
reg [2:0] state;

always @(posedge clk) begin
    if (!rstn) begin
        state <= RESET;
    end
    else begin
        case (state)
            IDLE    : state <= RUN;
            RUN     : state <= (req_reg && !hit_data) ? MISS : RUN;
            MISS    : state <= arready ? FILL : MISS;
            FILL    : state <= (axi_datacome && rlast) ? FINISH : FILL;
            FINISH  : state <= RUN;
            RESET   : state <= (reset_cnt == 7'd127) ? IDLE : RESET;
            default : state <= RESET;
        endcase
    end
end
```

使用verilog attribute `(* full_case, parallel_case *)`可以让case语句变成并行的而且不需要default语句。具体可见ISE/Vivado手册。

```{note}
1. 这三种语句都是有优先级的
2. `if-else`和`case`推荐只在时序逻辑中使用
```

### 顺序代码块

`begin-end`组合代表了这个代码块的各行代码是顺序执行的，这种代码块称为顺序代码块。

```verilog
always @(posedge clk) begin
    if (src_b) begin
        a <= b;
    if (src_c) begin
        a <= c;
    end
end
```

在时钟上升沿，如果`src_b`, `src_c`同时为1，那么`a`会被赋值为`c`，这是因为顺序代码块的各行代码是顺序执行的。这样`src_c`的优先级更高。如果想要`src_b`优先，只要像前面的例子使用嵌套`if-else`语句就行了。

### 模块声明和例化

模块被包含在关键字`module`、`endmodule`之内。

```verilog
module adder (          // 模块名称声明
    input  [31:0] a,    // 输入输出声明
    input  [31:0] b,
    output [31:0] c
);
    assign c = a + b;   // 变量声明、always语句、assign语句等
endmodule               // 模块结束
```

在实现CPU的顶层代码时，需要进行大量的模块例化，这里给出了一个例子进行说明。

当进行模块的端口声明时，如果没有明确指出其类型，那么这个端口会被隐含地声明为wire类型。

如定义一个模块`uart_tx`:

```verilog
module uart_tx (
    input       clk,
    input       valid,
    ...

    output      ready,
    output reg  txd
)

...

endmodule
```

我们在模块`uart_top`中例化这个模块，并命名为`U_uart_tx`：

```verilog
module uart_top (
    input        clk,
    input  [7:0] tx_data,
    input        tx_valid,
    ...

    input        rxd,
    output       txd,
    ...
)

wire tx_start, tx_ready;

// uart_top调用uart_tx，将其实例化为U_uart_tx
uart_tx U_uart_tx (
    .clk        (clk),
    .valid      (tx_start),
    ...
    .ready      (tx_ready),
    .txd        (txd)
)

...

endmodule
```

### Generate块

这里只介绍`generate for`块。

`generate for`的主要功能就是对模块或组件以及`always`块、`assign`语句进行复制。

使用 `generate for`的时候,必须要注意以下几点要求
* 在使用`generate for`的时候必须先声明一个genvar变量，用作for的循环变量。`genvar`是generate语句中的一种变量类型，用于在`generate for`语句中声明一个正整数的索引变量。
* `for`里面的内嵌语句,必须写在`begin-end`里
* 尽量对`begin-end`顺序块进行命名

`generate for`的语法示例如下：

```verilog
genvar i;
generate for (i = 0; i < 4; i = i + 1) begin: gen_assign_temp
    assign temp[i] = indata[2 * i + 1 : 2 * i];
end
endgenerate
```

### 编译指令

Verilog具有一些编译指令，它们的基本格式为`` `<keyword> ``，注意第一个符号不是单引号，而是键盘上数字1左边那个键对应的撇号。

常用的编译指令有文本宏预定义`` `define ``, `` `include``，它们的功能与C语言中类似，分别提供文本替换、文件包含的功能。

Verilog还提供了`` `ifdef``, `` `ifndef``等一系列条件编译指令，设计人员可以使得代码在满足一定条件的情况下才进行编译。

此外，`` `timescale ``指令可以对时间单位进行定义。例如：`` `timescale 1ns / 1ps ``。

### Attribute

Verilog的Attribute通常用于对综合工具进行更细粒度的控制。比如说实现乘法，既可以用FPGA上的通用资源（查找表，触发器等）实现，也可以用专用的DSP模块实现，使用Attribute可以指导综合工具选择我们希望的方案。

Attribute在变量、模块声明前，修饰该变量或模块。

举个常用的例子：`(* keep = "true" *) wire  sig1;`，该语句告诉综合工具，在综合时不要将其优化掉。

## 测试代码设计

测试编写的Verilog模块`xxx`，通常需要编写一个测试模块`tb_xxx`调用它并测试，该模块不会参与综合，可以使用些不可综合代码以方便测试编写。

### 时延语句

仿真中还经常使用时延来构造合适的仿真激励。它是不可综合的，仅能够在仿真中使用。时延分两类，一是语句内部时延，二是语句间时延，其示例如下所示：

```verilog
// 语句内部时延
A = #5 1'b1;

// 语句间时延
begin
    Temp = 1'b1;
    #5 A = Temp;
end
```

两种方式都表达在五个时间单位后，将A的值赋为1。

### initial语句

一般用来生成复位信号和激励。只在仿真开始时执行一次。

例如：
```verilog
// 测试代码中
// 假设`timescale 1ns / 1ps
reg rst_n, clk;

always #5 clk = ~clk;

initial begin
    rst_n = 1'b0;
    clk   = 1'b0;
#50 rst_n = 1'b1; 
end
```
会生成一个100MHz的时钟，一个50ns有效的复位信号。

```{warning}
虽然initial在FPGA上可以综合，但是在ASIC上不可综合。因此不要在设计代码中使用initial语句，一般寄存器初始化应使用复位信号
```

### 系统任务

系统任务可以被用来执行一些系统设计所需的输入、输出、时序检查、仿真控制操作。所有的系统任务名称前都带有符号`$`使之与用户定义的任务和函数相区分。

常见的系统任务：

* `$display`：用于显示指定的字符串，然后自动换行（用法类似C语言中的printf函数）
* `$time`：可以提取当前的仿真时间
* `$stop`：暂停仿真
* `$finish`：终止仿真
* `$random`：生成随机数
* `$readmemh`：读入一个16进制数的文件以初始化`reg`

### 测试设计

测试最基本的结构包括信号声明、激励和模块例化。

测试模块声明时，一般不需要声明端口。因为激励信号一般都在测试模块内部，没有外部信号。

声明的变量应该能全部对应被测试模块的端口。当然，变量不一定要与被测试模块端口名字一样。但是被测试模块输入端对应的变量应该声明为`reg`型，输出端对应的变量应该声明为`wire`型。

仿真过程中可以使用`$display`显示当前仿真进度或者测试结果。

在测试完成或者发现错误时可以使用`$finish;`或者`$stop;`来停止仿真。

## 推荐的编码风格

* 使用代码对齐、宏定义、参数增强可读性
* `module`, `wire`, `reg`等变量小写，`parameter`, `` `define ``一般大写
* 低有效的信号加上`_n`后缀，例如`rst_n`
* 模块例化名用`U_name[_number]`标识，多次例化加上序号0, 1, 2等。如：`U_icache`, `U_tag_0`

## 参考资料

1. Verilog. 维基百科, 自由的百科全书: <https://zh.wikipedia.org/w/index.php?title=Verilog>
2. Verilog 教程. 菜鸟教程: <https://www.runoob.com/w3cnote/verilog-tutorial.html>
3. Synthesis and Simulation Design Guide(UG626). Xilinx: <https://www.xilinx.com/support/documentation/sw_manuals/xilinx14_7/sim.pdf>
