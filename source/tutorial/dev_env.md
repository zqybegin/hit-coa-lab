# 开发环境的搭建

使用Nexys3开发板前，请确保已经安装Xilinx ISE 14.7版本。

请确保ISE的安装目录没有非ASCII字符（中文等）。

我们推荐使用的工具有：

* Visual Studio Code，现代化的代码编辑工具
* VS Code插件: [Verilog-HDL](https://marketplace.visualstudio.com/items?itemName=mshr-h.VerilogHDL)
* [Icarus Verilog](http://iverilog.icarus.com/)或者Verilator on WSL，除了仿真外，二者都可以配合插件进行语法检查
* Ctags, 配合插件可以一键例化，减少工作量

```{note}
由于ISE早已停止维护，Xilinx ISE 14.7不兼容Windows 10，某些操作会导致其闪退。虽然Xilinx的官网上有“面向Win10的ISE 14.7”，但其实是一个按照了ISE的Linux虚拟机，操作并不方便。

可以按照该网页[AR# 62380](https://www.xilinx.com/support/answers/62380.html)上“ISE 14.7 64-bit - Turning off SmartHeap”一节操作。这样应该能解决打开文件时崩溃的问题了。
```
