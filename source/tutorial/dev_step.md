# ISE 14.7 使用说明

本章以一个简单的流水灯实验介绍Xilinx ISE的设计流程、UCF（用户约束文件）的用法和作用，初步掌握实用Verilog HDL硬件描述语言进行简单的逻辑设计，熟悉Nexys3 FPGA开发板的使用。

## FPGA设计流程简介

FPGA即现场可编程门阵列，不过FPGA内部最小单元其实并不是一个个与或非门，而是一些更高层的模块（查找表、触发器、进位链等）。

ISE的作用是，将我们的Verilog源码映射到FPGA芯片上，用一种类似搭积木的方式实现我们的设计。

和编译一个C程序需要编译链接一样，编译一个Verilog设计也需要若干步骤：

* 第一步： 综合(Synthesize)，将HDL语言、原理图等设计输入翻译成由与、或、非门和RAM、触发器等基本逻辑单元的逻辑连接（网表），并根据目标和要求（约束条件）优化所生成的逻辑连接。
* 第二步：实现（Implementation）。将综合输出的逻辑网表翻译成所选器件的底层模块与硬件原语，将设计映射到器件结构上，进行布局布线，达到在选定器件上实现设计的目的。

其中实现又可分为3个步骤：翻译（Translate）逻辑网表，映射（Map）到器件单元与布局布线（Place & Route）。
* 翻译：将综合输出的逻辑网表翻译为 Xilinx特定器件的底层结构和硬件原语
* 映射：将设计映射到具体型号的器件上（LUT、 FF、 Carry 等）。
* 布局布线：根据用户约束和物理约束，对设计模块进行实际的布局，并根据设计连接，对布局后的模块进行布线，产生比特流文件。

目前多数FPGA的逻辑块都成二维阵列状排列，因此逻辑块布局问题可以被视为二次分配问题，是一种NP问题，因此只能使用模拟退火算法等方法得到符合要求的近似解（就算如此耗时也很长）。这就是为什么我们将管脚分配、时钟定义等称为约束。

## 使用ISE进行FPGA设计流程

### 建立新工程

* 打开ISE，创建新工程：

```{image} ../_static/img/tutorial/new-project.jpg
:alt: new-project
:width: 70%
:align: center
```

* 选择FPGA型号、综合和仿真工具、推荐描述语言等配置：

```{image} ../_static/img/tutorial/project-setting.jpg
:alt: project-setting
:width: 70%
:align: center
```

### 添加/编写代码

点击Project-Add Source添加已经编写好的模块与约束文件，也可以在Project-New Source处添加新的代码：

```{image} ../_static/img/tutorial/add-sources.jpg
:alt: add-sources
:width: 70%
:align: center
```

各个文件内容如下：

```verilog
// blink.v
`timescale 1ns / 1ps

module blink (
    input       clk  ,
    input       reset,

    output [7:0] leds
);

    reg [7 :0] leds_reg;
    reg [26:0] cnt;

    assign leds = leds_reg;

    // clock freq = 100MHz
    always @(posedge clk) begin
        if (reset) begin
            leds_reg <= 7'b1;
            cnt      <= 27'd0;
        end else if (cnt == 27'd100000000) begin
            cnt      <= 27'd0;
            leds_reg <= {leds_reg[6:0], leds_reg[7]};
        end else begin
            cnt      <= cnt + 27'd1;
        end
    end

endmodule
```

```verilog
// blink_tb.v
`timescale 1ns / 1ps

module blink_tb;

    reg clk, reset;
    wire [7:0] leds;

    initial begin
        clk = 0;
        reset = 1;
    #50 reset = 0;
    end

    always #5 clk = ~clk;

    blink u_blink(
        .clk   (clk   ),
        .reset (reset ),
        .leds  (leds  )
    );

endmodule
```

```
## blink_constr.ucf
## Clock signal
NET "clk"            LOC = "V10" | IOSTANDARD = "LVCMOS33";

## Buttons
NET "reset"          LOC = "B8"  | IOSTANDARD = "LVCMOS33";

## Leds
NET "leds<0>"        LOC = "U16" | IOSTANDARD = "LVCMOS33";
NET "leds<1>"        LOC = "V16" | IOSTANDARD = "LVCMOS33";
NET "leds<2>"        LOC = "U15" | IOSTANDARD = "LVCMOS33";
NET "leds<3>"        LOC = "V15" | IOSTANDARD = "LVCMOS33";
NET "leds<4>"        LOC = "M11" | IOSTANDARD = "LVCMOS33";
NET "leds<5>"        LOC = "N11" | IOSTANDARD = "LVCMOS33";
NET "leds<6>"        LOC = "R11" | IOSTANDARD = "LVCMOS33";
NET "leds<7>"        LOC = "T11" | IOSTANDARD = "LVCMOS33";
```

### 仿真

* View一栏选中Simulation，Hierarchy栏中选中`blink_tb`，双击Simulate Behavioral Model，等待ISim启动，观察仿真波形：

```{image} ../_static/img/tutorial/sim-start.jpg
:alt: sim-start
:width: 40%
:align: center
```

* 将Objects窗口的信号拖入波形窗口以观察新的信号。

```{image} ../_static/img/tutorial/sim-wave.jpg
:alt: sim-wave
:width: 80%
:align: center
```

如果没有Objects窗口，菜单栏勾选View-Panels-Objects打开它。

仿真没有问题后，可以开始下面的步骤。

### 综合与实现

* 在Hierarchy栏中选择blink.v顶层文件，在Process栏中双击Generate Programming File，生成配置比特流，该操作包含了上述提到的综合和实现过程。

* 将电脑与开发板USB PROG端口连接，打开Nexys3电源，双击Process栏中的Manage Configuration Project(iMPACT)进入IMPACT工具。

```{image} ../_static/img/tutorial/gen-bits.jpg
:alt: gen-bits
:width: 40%
:align: center
```

* 双击左侧栏中的 Boundary Scan，然后右键点击右侧窗口，选择Initialize Chain，可以扫描到FPGA芯片：

```{image} ../_static/img/tutorial/impact-init.jpg
:alt: impact-init
:width: 80%
:align: center
```

* 选择刚刚生成的配置比特流：

```{image} ../_static/img/tutorial/impact-select.jpg
:alt: impact-select
:width: 50%
:align: center
```

* 右键选中FPGA芯片，点击Program进行下载：

```{image} ../_static/img/tutorial/impact-program.jpg
:alt: impact-program
:width: 40%
:align: center
```

* 观察到开发板上的DONE LED亮起说明下载成功，同时可以观察到8个LED构成的流水灯依次闪烁。

### 硬件调试

* 首先点击Synthesize-XST把整个工程综合一遍。
* 鼠标右键顶层模块blink，选择New Source，在弹出的界面中选择ChipScope Definition and Connection File选项。

```{image} ../_static/img/tutorial/add-chipscope-file.jpg
:alt: add-chipscope-file
:width: 80%
:align: center
```

* 新建好文件后双击它，弹出ChipScope Pro Core Inserter，点击2次Next（都使用默认设置）：

```{image} ../_static/img/tutorial/chipscope-config1.jpg
:alt: chipscope-config1
:width: 80%
:align: center
```

* 选择触发端口的数目和宽度：

```{image} ../_static/img/tutorial/chipscope-config2.jpg
:alt: chipscope-config2
:width: 80%
:align: center
```

* 选择捕捉信号的深度和一些其他配置，**注意深度不要太深**。这里我们用复位信号触发捕捉，所以不勾选“Data Same As Trigger”：

```{image} ../_static/img/tutorial/chipscope-config3.jpg
:alt: chipscope-config3
:width: 80%
:align: center
```

* 点击Modify Connections，开始连线：

```{image} ../_static/img/tutorial/chipscope-config-over.jpg
:alt: chipscope-config-over
:width: 80%
:align: center
```

* 配置时钟信号，左下方选择时钟信号，点击Make Connections连接：

```{image} ../_static/img/tutorial/chipscope-config-clk.jpg
:alt: chipscope-config-clk
:width: 80%
:align: center
```

* 配置触发信号：

```{image} ../_static/img/tutorial/chipscope-config-trigger.jpg
:alt: chipscope-config-trigger
:width: 80%
:align: center
```

* 配置抓取的数据：

```{image} ../_static/img/tutorial/chipscope-config-data.jpg
:alt: chipscope-config-data
:width: 80%
:align: center
```

* 配置完成后，Net Connections页应该没有红色的标记，点击“Return to Project Navigator”并选择保存。

* 在Process栏中双击Generate Programming File，生成配置比特流。

* 双击Analyze Design Using ChipScope，点击红色部分后连接，再点击OK确定：

```{image} ../_static/img/tutorial/chipscope-init.jpg
:alt: chipscope-init
:width: 80%
:align: center
```

* 右键左上角的JTAG Chain下的DEV:0，选择Configure，点击OK：

```{image} ../_static/img/tutorial/chipscope-select.jpg
:alt: chipscope-select
:width: 30%
:align: center
```

* 双击Trigger Setup和Waveform分别打开触发设置和波形窗口，配置触发条件，观察波形：

```{image} ../_static/img/tutorial/chipscope-wave.jpg
:alt: chipscope-wave
:width: 80%
:align: center
```

## 参考资料

1. [ISE Design Suite Overview](https://www.xilinx.com/html_docs/xilinx14_7/isehelp_start.htm)
2. [ISim User Guide](https://www.xilinx.com/support/documentation/sw_manuals/xilinx14_7/plugin_ism.pdf)
3. [ISE Tutorial: Using Xilinx ChipScope Pro ILA Core with Project Navigator to Debug FPGA Applications](https://www.xilinx.com/support/documentation/sw_manuals/xilinx14_7/ug750.pdf)
