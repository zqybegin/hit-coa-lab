# HIT COA Lab

[![pipeline status](https://gitlab.com/hit-coa/hit-coa-lab/badges/master/pipeline.svg)](https://gitlab.com/hit-coa/hit-coa-lab/-/commits/master)

哈尔滨工业大学《计算机组织与体系结构》实验指导书。

## 1. 链接

该项目使用GitLab CI自动生成网页和PDF文件，通过GitLab Pages托管。

网页地址：[hit-coa-lab](https://hit-coa.gitlab.io/hit-coa-lab/)

校内镜像站：[hit-coa-lab](https://mirrors.hit.edu.cn/hit-coa-lab/)（感谢 [@HITLUG](https://lug.hit.edu.cn/)）

PDF文件：[manual.pdf](https://hit-coa.gitlab.io/hit-coa-lab/manual.pdf)

## 2. 语法

`source/index.rst`包含了该文档的组织结构，使用reStructuredText (reST)标记语言。

语法参考：<https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html>

`source`目录下的`*.md`文件为Markedly Structured Text(MyST)文件，使用Markdown的超集以方便拓展。

语法参考：<https://myst-parser.readthedocs.io/en/latest/index.html>

## 3. 本地编译

1. 首先确保已经安装Python 3，然后安装以下包：

```bash
pip install sphinx
pip install sphinx_rtd_theme
pip install myst-parser
```

2. 切换到本项目根目录下，运行`make html`生成网页

3. 如果要生成PDF，还需要安装TexLive。

   * 直接生成PDF文件：```sphinx-build -M latexpdf source dest```或者`make latexpdf`

   * 生成Tex文件以便之后使用：```sphinx-build -M latex source dest```

## 4. 配置步骤

### 1. 配置GitLab Runner

GitLab原本提供了共享的Runner来做CI，但是由于其经常被挖矿程序滥用，因此现在要使用GitLab的共享Runner的话，必须提供信用卡信息。在这里我们直接自行搭建GitLab-Runner。

配置过程如下所示：

<https://docs.gitlab.com/runner/install/linux-manually.html#using-binary-file>

最后一步要注册GitLab Runner，按照该说明操作：

<https://docs.gitlab.com/runner/register/index.html#linux>

### 2. 安装Docker Engine

由于上一步我们的runner executor选择的是Docker，所以还需要安装Docker Engine。

对于Ubuntu，按照官方说明操作：

<https://docs.docker.com/engine/install/ubuntu/>

该步骤完成后，CI应该可以正常工作了。

### 3. 使用镜像

主要使用2个镜像：

* `python:3.8-alpine`：用于安装Sphinx和插件；
* `texlive/texlive:latest`：用于生成PDF文件；

## 5. 更新历史

* 2021/09/xx: 初始版本

## 6. 贡献者

* 舒燕君
* 马庄宇
* 王万优
* 张清钰 [@zqybegin](https://gitlab.com/zqybegin)
* 代昆 [@d4nk3n](https://gitlab.com/d4nk3n) (Maintainer)
